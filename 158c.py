p = []
for _ in xrange(int(raw_input())):
    t = raw_input().split()
    if t[0] == 'pwd':
        print '/'+'/'.join(p)+('','/')[len(p)>0]
    else:
        for d in t[1].split('/'):
            if d == '': p = []
            elif d == '..': p.pop()
            else: p.append(d)
