use std::io;

fn main() {
    let mut ns = String::new();
    io::stdin().read_line(&mut ns).unwrap();
    let n: u16 = ns.trim().parse::<u16>().unwrap();
    let nums = [4,7,44,47,74,77,444,447,474,477,744,747,774,777];
    for ni in nums.iter() {
        if n % ni == 0 {
            println!("YES");
            return;
        }
    }
    println!("NO");
    return;
}
