calc k a
  | length a == 0 = []
  | length a == 1 = a
  | length a > 1 && k-(a!!0)-(a!!1) > 0 = [a!!0] ++ calc k ([k-(a!!0)]++(drop 2 a))
  | otherwise = [a!!0] ++ calc k (drop 1 a)

f(_:k:a) = [[foldl (+) 0 [bi - ai | (ai, bi) <- zip a b]], b] where b = calc k a

main=interact$unlines.map(unwords.map show).f.map read.words

