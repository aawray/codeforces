lines = IO.readlines("input.txt")

#n = lines[0].to_i
cards = []
lines[1].split(" ").each_with_index do |val,ind|
  (cards[val.to_i] ||= []) << ind.to_i+1
end

cards.compact!

File.open("output.txt", "w") do |fout|
  if cards.count{|p| p.size%2!=0 if p} != 0
    fout.puts("-1")
  else
    cards.each do |pairs|
      pairs.each_slice(2) do |pair|
        fout.puts("#{pair[0]} #{pair[1]}")
      end if pairs
    end
  end
end

