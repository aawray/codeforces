l = []; r = []
n = int(raw_input())
for _ in xrange(n): li, ri = map(int, raw_input().split()); l.append(li); r.append(ri)
print min(l.count(0), l.count(1))+min(r.count(0), r.count(1))
