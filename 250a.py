n = int(raw_input())
a = map(int, raw_input().strip().split())

b = [0 for _ in xrange(n)]; i = 0; c = 0
for ai in a:
    if ai < 0:
        c += 1
    if c == 3:
        c = 1; i += 1
    b[i] += 1

nb = [str(bi) for bi in b if bi > 0]

print len(nb)
print ' '.join(nb)