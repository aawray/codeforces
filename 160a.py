n = raw_input()
ns = map(int, raw_input().strip().split())
ns.sort(reverse=1)
a1 = 0; a2 = sum(ns); cnt = 0
for x in ns:
    if a1 <= a2:
        a1 += x
        a2 -= x
        cnt += 1
    else:
        break
print cnt