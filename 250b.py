n = int(raw_input())
for _ in xrange(n):
    bts = raw_input().split(':')
    zr = 8 - len(bts) + bts.count('')
    zr_f = 0
    s = []
    for bt in bts:
        if bt == '':
            if not zr_f:
                for _ in xrange(zr): s.append("0000")
                zr_f = 1
        else: s.append("0"*(4-len(bt))+bt)
    print ':'.join(s)