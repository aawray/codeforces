import re
from math import pow

for _ in xrange(int(raw_input())):
    s = raw_input()
    r = re.findall("R([0-9]+)C([0-9]+)", s)
    if len(r) > 0:
        row, col = map(int, r[0]); s_col = ''
        while col: s_col += chr((col-1)%26+65); col = (col - 1)/26
        print s_col[::-1]+row.__str__()
    else:
        r = re.findall("([A-Z]+)([0-9]+)", s)
        if len(r) > 0:
            col, row = r[0]; s_col = 0
            for i in xrange(0, len(col)): s_col += (ord(col[i])-64)*int(pow(26, len(col)-i-1))
            print ''.join(['R', row.__str__(), 'C', s_col.__str__()])
