n,t=$<.map{|x|x.strip}
n=n.to_i-1

loop do
  x,_,t=t.partition ' '
  ts=t
  loop do
    ts,_,y=ts.rpartition ' '
    break if ts.empty?
    xy=((x+y)/2.0).to_s.gsub '.0',''
    (puts "YES"; exit) if t.include? xy
  end
  break if x.empty?
end

puts "NO"
