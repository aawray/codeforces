use std::fs::File;
use std::io::Read;
use std::io::BufReader;
use std::io::BufRead;
use std::io::Write;
use std::str::FromStr;

fn main() {
    let mut f_in = File::open("input.txt").unwrap();
    let mut f_out = File::create("output.txt").unwrap();

    let mut f_in_reader = BufReader::new(f_in);

    let mut f_in_lines = f_in_reader.lines();

    //let n = usize::from_str(f_in_lines.next().unwrap().unwrap().as_str()).unwrap();
    f_in_lines.next();

    let mut cards: Vec<Vec<u16>> = (0..5001).map(|_| Vec::new()).collect();

    let mut i: u16 = 1;
    for x in f_in_lines.next().unwrap().unwrap().split_whitespace() {
        let ni: usize = x.parse::<usize>().unwrap();
        cards[ni].push(i);
        i += 1;
    }

    for pairs in &cards {
        if pairs.len() % 2 != 0 {
            write!(f_out, "-1");
            return;
        }
    }

    for pairs in &cards {
        let mut j: usize = 0;
        while j < pairs.len() {
            write!(f_out, "{} {}\n", pairs[j], pairs[j+1]);
            j += 2;
        }
    }

    return;
}

