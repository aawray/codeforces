n, k = map(int, raw_input().split())
a =  map(int, raw_input().split())

p = [i for i in xrange(1, n*k+1)]

r = []
for _ in xrange(k):
    ai = a.pop(0)
    p.remove(ai)
    t = [str(ai)]
    while len(t) < n:
        pi = p.pop(0)
        if pi not in a:
            t.append(str(pi))
        else:
            p.append(pi)
    r.append(' '.join(t))
print ' '.join(r)
