n = int(raw_input())

r = set()

def tt(a, x, y):
    if a <= n:
        r.add(a)
        if (a * 10 + x != a): tt(a * 10 + x, x, y)
        if (a * 10 + y != a): tt(a * 10 + y, x, y)

for x in xrange(1, 10):
    for y in xrange(0, x+1):
        tt(0, x, y)

print len(r)-1