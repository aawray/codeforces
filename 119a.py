from fractions import gcd
a, b, n = map(int, raw_input().strip().split(' ')); p = 1
while n > 0: n -= gcd(n, a); p ^= 1; a, b = b, a
print(p)