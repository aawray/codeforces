raw_input()
i = 1
pl = []
for k in map(int, raw_input().split()):
    pl.append((i, k))
    i += 1

pl = sorted(pl, key = lambda x: x[1])

#t1 = [str(p[0]) for p in pl[::2]]
#t2 = [str(p[0]) for p in pl if str(p[0]) not in t1]

t1 = pl[::2]
print len(t1)
print ' '.join([str(p[0]) for p in t1])

t2 = set(pl)-set(t1)
print len(t2)
print ' '.join([str(p[0]) for p in t2])
