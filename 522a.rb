gets
w = {}
while(ln = gets)
  dst, _, src = ln.downcase.split(' ')
  w[dst] = (w[src] || 1) + 1
end
puts w.values.max
