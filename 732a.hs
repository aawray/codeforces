xs k r x | (k*x) `rem` 10 == 0 || (k*x-r) `rem` 10 == 0 = x | otherwise = xs k r (x+1)
s[k,r] = xs k r 1
main = interact$show.s.map read.words
